
import React, { Component } from 'react';
import { Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Content, Item, Input, Button, Icon, View, Text, Picker, Spinner } from 'native-base';
import axios from 'axios';

import { setUser } from '../../actions/user';
import styles from './styles';

const {
  replaceAt,
} = actions;

const appConfig = require('../../app-config.json');
const loginScreenBackground = require('../../images/Status-battery-charging-caution-icon_medium.png');

const phoneNumberLength = 10;

const phoneNumberPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

class Login extends Component {
  static propTypes = {
    setUser: React.PropTypes.func,
    replaceAt: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: '',
      userType: 'T',
      firstName: '',
      profileId: undefined,
      loading: false,
      error: '',
    };
  }


  onLoginFail() {
    this.setState({
      phoneNumber: '',
      userType: 'T',
      firstName: '',
      profileId: '',
      loading: false,
      error: 'Authentication failed',
    });
  }

  onLoginSuccess() {
    this.setState({
      phoneNumber: '',
      userType: 'T',
      firstName: '',
      profileId: '',
      loading: false,
      error: '',
    });
  }

  onLoginPress() {
    const { phoneNumber } = this.state;
    if (phoneNumber.length !== phoneNumberLength
      && !phoneNumberPattern.test(phoneNumber)) {
      Alert.alert('Warning', 'Invalid phone number.');
      return;
    }
    this.setState({
      loading: true,
      error: '',
    });

    this.fetchProfile();
  }

  setUser(phoneNumber, userType, firstName, profileId) {
    this.props.setUser(phoneNumber, userType, firstName, profileId);
    this.props.replaceAt('login', { key: 'home' }, this.props.navigation.key);
  }

  fetchProfile() {
    axios.get(`${appConfig.lithoserBaseURL}/lithoser/profile/detailsByPhoneNumber/${
      this.state.phoneNumber}/${this.state.userType}`)
        .then((response) => {
          const profileDetails = response.data.profileDetails;
          this.setUser(profileDetails.phoneNumber, profileDetails.profileType,
            profileDetails.firstName, profileDetails.profileId);
          this.onLoginSuccess.bind(this);
        })
        .catch(() => {
          this.onLoginFail.bind(this);
        }
      );
  }

  renderLoginButton() {
    if (this.state.loading) {
      return <Spinner size="large" color="blue" />;
    }

    return (
      <Button iconLeft rounded success style={styles.btn} onPress={() => { this.onLoginPress(); }} >
        <Icon name="log-in" />
        <Text>Login</Text>
      </Button>
    );
  }

  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Content>
            <Image source={loginScreenBackground} style={styles.loginScreenBackground}>
              <View style={styles.bg}>
                <Item style={styles.input}>
                  <Icon active name="person" />
                  <Input
                    placeholder="PHONE#"
                    value={this.state.phoneNumber}
                    onChangeText={phoneNumber => this.setState({ phoneNumber })}
                    keyboardType="numeric"
                    maxLength={10}
                  />
                </Item>
                <Picker
                  style={styles.input}
                  iosHeader="Select one"
                  mode="dropdown"
                  selectedValue={this.state.userType}
                  onValueChange={userType => this.setState({ userType })}
                >
                  <Item label="Technician" value="T" />
                  <Item label="Customer" value="C" />
                  <Item label="Admin" value="A" />
                </Picker>
                { this.renderLoginButton() }
              </View>
            </Image>
          </Content>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    replaceAt: (routeKey, route, key) => dispatch(replaceAt(routeKey, route, key)),
    setUser: (phoneNumber, userType, firstName, profileId) =>
              dispatch(setUser(phoneNumber, userType, firstName, profileId)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindActions)(Login);
