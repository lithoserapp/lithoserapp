export default {
  container: {
    backgroundColor: '#FBFAFA',
  },
  row: {
    flex: 1,
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: 'center',
  },
  mt: {
    marginTop: 18,
  },
  dateIconStyle: {
    position: 'absolute',
    left: 0,
    top: 4,
    marginLeft: 0,
  },
  btn: {
    alignSelf: 'flex-start',
  },
  trashBtn: {
    left: 5,
    marginTop: 5,
    alignSelf: 'flex-start',
  },
};
