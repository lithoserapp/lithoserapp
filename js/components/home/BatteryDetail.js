import React from 'react';
import { Alert } from 'react-native';
import { Text, Left, Body, Button, List, ListItem,
   Right, Thumbnail, Icon } from 'native-base';
import * as Progress from 'react-native-progress';
import styles from './styles';

const defaultBatteryImage = require('../../../images/battery.png');

const BatteryDetail = ({ batteryProp, onBatteryItemDelete }) => {
  let progress = 0.3;
  const timeDiff = Math.abs(batteryProp.installedDate - new Date());
  const dayDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
  progress = dayDiff / batteryProp.lifeExpectancyInDays > 1
             ? 1
             : dayDiff / batteryProp.lifeExpectancyInDays;
  let color = 'green';
  if (progress > 0.9) {
    color = 'red';
  } else if (progress > 0.6 && progress < 0.9) {
    color = 'yellow';
  } else if (progress < 0.6) {
    color = 'green';
  }
  const progressInPercentage = Math.round(progress * 100);
  const currentBatteryImage = batteryProp.batteryImage || defaultBatteryImage;
  const onItemDetailPress = () => {
    Alert.alert('onItemDetailPress');
  };
  const onItemDeletePress = () => {
    onBatteryItemDelete(batteryProp.saleDetailsId);
  };
  return (
    <List>
      <ListItem avatar itemDivide button onPress={onItemDetailPress}>
        <Left>
          <Thumbnail square source={currentBatteryImage} />
        </Left>
        <Body>
          <Text>Contact: {batteryProp.customerPhoneNumber}</Text>
          <Text note>{batteryProp.batteryName} Usage: {progressInPercentage}%</Text>
          <Progress.Bar
            progress={progress}
            width={230}
            height={10}
            color={color}
          />
        </Body>
        <Right>
          <Text note> Qty: {batteryProp.quantity} </Text>
          <Button
            iconRight small transparent warning onPress={onItemDeletePress}
            style={styles.trashBtn}
          >
            <Icon active name={'trash'} />
          </Button>
        </Right>
      </ListItem>
    </List>
  );
};
BatteryDetail.propTypes = {
  onBatteryItemDelete: React.PropTypes.func.isRequired,
  batteryProp: React.PropTypes.object.isRequired,
};
export default BatteryDetail;
