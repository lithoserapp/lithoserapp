
import React, { Component } from 'react';
import { TouchableOpacity, Alert, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import { Container, Header, Title, Content, View, Text, Button, Icon, Left, Body, Picker,
  Item, List, ListItem, InputGroup, Input, Right, Footer, FooterTab, Thumbnail, Spinner } from 'native-base';
import axios from 'axios';
import DatePicker from 'react-native-datepicker';

import { openDrawer } from '../../actions/drawer';
import { setIndex } from '../../actions/list';
import styles from './styles';
import BatteryDetail from './BatteryDetail';

const {
  reset,
  pushRoute,
} = actions;

const appConfig = require('../../app-config.json');
const smallBatteryIcon = require('../../images/battery-icon.png');

const phoneNumberLength = 10;
const phoneNumberPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

class Home extends Component {
  static propTypes = {
    phoneNumber: React.PropTypes.string,
    userType: React.PropTypes.string,
    profileId: React.PropTypes.number,
    setIndex: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    reset: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }
  constructor(props) {
    super(props);

    this.state = {
      batteryListPage: true,
      newCustomer: false,
      profilePage: false,
      settingPage: false,
      firstName: '',
      middleName: '',
      lastName: '',
      userName: '',
      userEmailId: '',
      addressStreetName: '',
      cityZipcode: '',
      userSex: 'Other',
      batteryList: [],
      loading: false,
      error: '',
    };
  }
  componentWillMount() {
    this.onBatteryListLoad();
  }

  onLoadFail() {
    this.setState({
      loading: false,
      error: 'Service call failed',
    });
  }

  onLoadSuccess(response) {
    this.setState({
      batteryList: response.data.batterySaleDetailsList,
      loading: false,
      error: '',
    });
  }
  onUserProfileLoad() {
    axios.get(`${appConfig.lithoserBaseURL}/lithoser/profile/detailsByProfileId/${
    this.props.profileId}`)
      .then((response) => {
        const profileDetails = response.data.profileDetails;
        let profileUserName = profileDetails.firstName || '';
        profileUserName = profileUserName && profileDetails.middleName ?
                  `${profileUserName}  ${profileDetails.middleName}` : profileUserName;
        profileUserName = profileUserName && profileDetails.lastName ?
                  `${profileUserName}  ${profileDetails.lastName}` : profileUserName;
        this.setState({
          userName: profileUserName,
          userEmailId: profileDetails.emailId || '',
          userSex: profileDetails.sex || 'Other',
          addressStreetName: profileDetails.addressStreetName || '',
          cityZipcode: profileDetails.cityZipcode || '',
        });
      })
      .catch((exception) => {
        console.log(exception);
      }
      );
  }
  onBatteryItemDelete(saleDetailsId) {
    axios.post(`${appConfig.lithoserBaseURL}/lithoser/battery/updateActiveStatusOfSale?activeStatus=N&saleDetailsId=${saleDetailsId}`)
      .then(() => {
        Alert.alert('Battery sale deleted successfully.');
        this.onBatteryListLoad();
      }
      )
      .catch((exception) => {
        console.log('Exception :');
        console.log(exception);
      }
      );
  }
  onBatteryListLoad(sellerProfileIdParam) {
    const sellerProfileId = sellerProfileIdParam || this.props.profileId;
    axios.get(`${appConfig.lithoserBaseURL}/lithoser/battery/saleDetailsListBySeller?sellerProfileId=${sellerProfileId}`)
    .then((response) => {
      console.log(response);
      this.onLoadSuccess(response);
    }
    )
    .catch((exception) => {
      console.log('Exception :');
      console.log(exception);
      this.onLoadFail();
    }
    );
  }
  onBatteryInfoFormReset() {
    this.setState(
      {
        batteryName: '',
        installedDate: '',
        batteryLife: 0,
        batteryModel: '',
        batteryQty: 0,
        customerPhoneNum: '',
      });
  }
  onSaveBatteryInfo() {
    const phoneNumber = this.state.customerPhoneNum;
    if (phoneNumber.length !== phoneNumberLength
      && !phoneNumberPattern.test(phoneNumber)) {
      Alert.alert('Warning', 'Invalid customer phone number.');
      return;
    }
    const data = {
      batteryName: this.state.batteryName || '',
      installedDate: this.state.installedDate || '',
      lifeExpectancyInDays: this.state.batteryLife || 0,
      batteryModel: this.state.batteryModel || '',
      quantity: this.state.batteryQty || 0,
      customerPhoneNumber: this.state.customerPhoneNum || '',
      soldByProfileId: this.props.profileId,
    };
    // FIXME include the profile creation or add profile Id to the sale
    // FIXME include comment field as well
    axios.post(`${appConfig.lithoserBaseURL}/lithoser/battery/createSale`, data)
      .then(
        Alert.alert('Battery sale register successfully')
      )
      .catch((exception) => {
        console.log('Exception :');
        console.log(exception);
      }
      );
  }
  onSaveProfileInfo() {
    const userName = this.state.userName.trim();
    const nameWordCount = userName.split(' ').length;
    const userFirstName = userName.split(' ')[0]
                      ? userName.split(' ')[0].trim()
                      : '';
    const userMiddleName = nameWordCount > 2 && userName.split(' ')[1]
                      ? userName.split(' ')[1].trim()
                      : '';
    let userLastName = '';
    if (userMiddleName) {
      userLastName = userName.split(' ').slice(2, nameWordCount).join(' ');
    } else {
      userLastName = userName.split(' ').slice(1, nameWordCount).join(' ');
    }

    const data = {
      firstName: userFirstName || '',
      middleName: userMiddleName || '',
      lastName: userLastName || '',
      emailId: this.state.userEmailId || '',
      phoneNumber: this.props.phoneNumber,
      dateOfBirth: null,
      sex: this.state.userSex || 'Other',
      addressStreetName: this.state.addressStreetName || '',
      cityZipcode: this.state.cityZipcode || '',
      profileType: this.props.userType,
      isPushNotificationEnabled: 'Yes',
      isEmailNotificationEnabled: 'Yes',
      activeStatus: 'Yes',
    };

    axios.post(`${appConfig.lithoserBaseURL}/lithoser/profile/update`, data)
      .then(Alert.alert('Profiled updated successfully'))
      .catch((exception) => {
        console.log('Exception :');
        console.log(exception);
      }
      );
  }

  setBatteryListPage() {
    this.setState({
      batteryListPage: true,
      newCustomer: false,
      profilePage: false,
      settingPage: false,
      batteryList: [],
      loading: true,
      error: '',
    });
    this.onBatteryListLoad();
  }

  setNewCustomerPage() {
    this.setState({
      batteryListPage: false,
      newCustomer: true,
      profilePage: false,
      settingPage: false,
    });
  }
  setProfilePage() {
    this.setState({
      batteryListPage: false,
      newCustomer: false,
      profilePage: true,
      settingPage: false,
    });
    this.onUserProfileLoad();//FIXME add spinner
  }
  setSettingPage() {
    this.setState({
      batteryListPage: false,
      newCustomer: false,
      profilePage: false,
      settingPage: true,
    });
  }

  pushRoute(route, index) {
    this.props.setIndex(index);
    this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
  }

  renderHelper() {
    if (this.state.loading) {
      return <Spinner size="large" color="blue" />;
    }
    return this.state.batteryList.map((batteryItem, index) =>
      <BatteryDetail
        key={index}
        batteryProp={batteryItem}
        onBatteryItemDelete={this.onBatteryItemDelete.bind(this)}
      />
    );
  }
  renderContent() {
    if (this.state.batteryListPage) {
      return (
        <Container>
          <Header searchBar rounded>
            <Item>
              <Icon name="search" />
              <Input
                placeholder="Search" keyboardType="numeric"
                maxLength={10}
              />
              <Icon name="contacts" />
            </Item>
            <Button transparent success onPress={() => { this.setBatteryListPage(); }}>
              <Icon name="refresh" />
            </Button>
          </Header>

          <ScrollView>
            {this.renderHelper()}
          </ScrollView>
        </Container>

      );
    } else if (this.state.newCustomer) {
      return (
        <Content>
          <TouchableOpacity>
            <Thumbnail
              size={80} source={smallBatteryIcon}
              style={{ alignSelf: 'center', marginTop: 20, marginBottom: 10 }}
            />
          </TouchableOpacity>
          <List>
            <ListItem>
              <InputGroup>
                <Input
                  placeholder="Battery Name"
                  value={this.state.batteryName}
                  onChangeText={batteryName => this.setState({ batteryName })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <DatePicker
                  style={{ width: 200 }}
                  date={this.state.installedDate}
                  mode="date"
                  placeholder="Installed Date"
                  format="YYYY-MM-DD"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: styles.dateIconStyle,
                    dateInput: {
                      marginLeft: 36,
                    },
                  }}
                  onDateChange={installedDate => this.setState({ installedDate })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Input
                  placeholder="Battery Life (in days)" value={this.state.batteryLife}
                  onChangeText={batteryLife => this.setState({ batteryLife })}
                  keyboardType="numeric"
                  maxLength={5}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  placeholder="Model" value={this.state.batteryModel}
                  onChangeText={batteryModel => this.setState({ batteryModel })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  placeholder="Battery Quantity" value={this.state.batteryQty}
                  onChangeText={batteryQty => this.setState({ batteryQty })}
                  keyboardType="numeric"
                  maxLength={5}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  placeholder="Customer Phone#" value={this.state.customerPhoneNum}
                  onChangeText={customerPhoneNum => this.setState({ customerPhoneNum })}
                  keyboardType="numeric"
                  maxLength={10}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <Button
                small iconLeft rounded success onPress={() => { this.onSaveBatteryInfo(); }}
                style={styles.btn}
              >
                <Icon name="recording" />
                <Text> Save </Text>
              </Button>
              <Button
                small iconLeft rounded warning onPress={() => { this.onBatteryInfoFormReset(); }}
                style={styles.btn}
              >
                <Icon name="refresh" />
                <Text> Reset </Text>
              </Button>
            </ListItem>
          </List>

        </Content>
      );
    } else if (this.state.profilePage) {
      return (
        <Content>
          <TouchableOpacity>
            <Thumbnail
              size={80} source={smallBatteryIcon}
              style={{ alignSelf: 'center', marginTop: 20, marginBottom: 10 }}
            />
          </TouchableOpacity>
          <List>
            <ListItem>
              <InputGroup>
                <Icon name="person" style={{ color: '#0A69FE' }} />
                <Input
                  inlineLabel label="Name" placeholder="Name"
                  value={this.state.userName}
                  onChangeText={userName => this.setState({ userName })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="mail" style={{ color: '#0A69FE' }} />
                <Input
                  placeholder="EMAIL" value={this.state.userEmailId}
                  onChangeText={userEmailId => this.setState({ userEmailId })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="call" style={{ color: '#0A69FE' }} />
                <Input
                  disabled
                  placeholder="PHONE"
                  value={this.props.phoneNumber}
                />
              </InputGroup>
            </ListItem>
            <ListItem iconLeft>
              <Icon name="transgender" style={{ color: '#0A69FE' }} />
              <Text>GENDER</Text>
              <Picker
                iosHeader="Select one"
                mode="dropdown"
                selectedValue={this.state.userSex}
                onValueChange={userSex => this.setState({ userSex })}
              >
                <Item label="Male" value="Male" />
                <Item label="Female" value="Female" />
                <Item label="Transgender" value="Transgender" />
                <Item label="Other" value="Other" />
              </Picker>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  stackedLabel label="Permanent Address" placeholder="Street Address, City, State"
                  value={this.state.addressStreetName}
                  onChangeText={addressStreetName => this.setState({ addressStreetName })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  placeholder="Zipcode"
                  value={this.state.cityZipcode}
                  keyboardType="numeric"
                  maxLength={10}
                  onChangeText={cityZipcode => this.setState({ cityZipcode })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <Button
                small iconLeft rounded success onPress={() => { this.onSaveProfileInfo(); }}
                style={styles.btn}
              >
                <Icon name="recording" />
                <Text> Save </Text>
              </Button>
            </ListItem>
          </List>

        </Content>
      );
    }

    return (
      <View />
    );
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={this.props.openDrawer}>
              <Icon active name="menu" />
            </Button>
          </Left>

          <Body>
            <Title> {'Lithoser'} </Title>
          </Body>

          <Right>
            <Button transparent onPress={() => this.props.reset(this.props.navigation.key)}>
              <Icon active name="exit" />
            </Button>
          </Right>
        </Header>

        <Content>
          { this.renderContent() }
        </Content>
        <Footer >
          <FooterTab>
            <Button active={this.state.batteryListPage} onPress={() => this.setBatteryListPage()} >
              <Icon name="battery-charging" />
              <Text>Batteries</Text>
            </Button>
            <Button active={this.state.newCustomer} onPress={() => this.setNewCustomerPage()} >
              <Icon name="add-circle" />
              <Text>New</Text>
            </Button>
            <Button active={this.state.profilePage} onPress={() => this.setProfilePage()} >
              <Icon name="person" />
              <Text>Profile</Text>
            </Button>
            <Button active={this.state.settingPage} onPress={() => this.setSettingPage()} >
              <Icon name="settings" />
              <Text>Settings</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    openDrawer: () => dispatch(openDrawer()),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
    reset: key => dispatch(reset([{ key: 'login' }], key, 0)),
  };
}

const mapStateToProps = state => ({
  phoneNumber: state.user.phoneNumber,
  userType: state.user.userType,
  profileId: state.user.profileId,
  firstName: state.user.firstName,
  list: state.list.list,
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Home);
