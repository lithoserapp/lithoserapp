
import type { Action } from '../actions/types';
import { SET_USER } from '../actions/user';

export type State = {
    phoneNumber: string,
    userType: string
}

const initialState = {
  phoneNumber: '',
  userType: '',
  firstName: '',
  profileId: '',
};

export default function (state:State = initialState, action:Action): State {
  if (action.type === SET_USER) {
    return {
      ...state,
      phoneNumber: action.payload.phoneNumber,
      userType: action.payload.userType,
      firstName: action.payload.firstName,
      profileId: action.payload.profileId,
    };
  }
  return state;
}
