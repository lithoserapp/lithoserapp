
import type { Action } from './types';

export const SET_USER = 'SET_USER';

export function setUser(phoneNumber:string, userType:string,
  firstName: string, profileId: number):Action {

  return {
    type: SET_USER,
    payload: { phoneNumber, userType, firstName, profileId },
  };
}
