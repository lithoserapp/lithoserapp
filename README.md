
## Lithoser 1.0.0

###(a). Installation

On the command prompt run the following commands

```sh
$ git clone https://bitbucket.org/lithoserapp/lithoserapp.git

$ cd lithoserapp/

$ npm install
```

###(b). Simulate for iOS

**Method One**

*	Open the project in XCode from **ios/lithoserapp.xcodeproj**

*	Hit the play button.


**Method Two**

*	Run the following command in your terminal

```sh
$ react-native run-ios
```

###(c). Simulate for Android

*	Make sure you have an **Android emulator** installed and running.

*	Run the following command in your terminal

```sh
$ react-native run-android
```
